/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Prueba;

import DAO.Conexion;
import DAO.PlanMovilJpaController;
import DTO.PlanMovil;
import java.util.List;

/**
 *
 * @author madar
 */
public class TestBD {

    public static void main(String[] args) {
        //1. Establecer la conexión a la BD
        Conexion c = new Conexion();
        PlanMovilJpaController plan = new PlanMovilJpaController(c.getBd());
        List<PlanMovil> planes = plan.findPlanMovilEntities();
        for (PlanMovil p : planes) {
            System.out.println(p.getCodigoPlan() + "," + p.getNombrePlan() + "," + p.getPrecio());
        }

    }
}
